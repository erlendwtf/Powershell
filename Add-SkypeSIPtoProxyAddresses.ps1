$ou = "the DN of your users OU"
$users = Get-ADUser -SearchBase $ou -Filter * -Properties proxyAddresses

ForEach ($user in $users) 
{
    $SIP = "SIP:$($user.UserPrincipalName)"
    if($SIP -notin $user.proxyAddresses)
    {
        Set-ADUser -Identity $user.DistinguishedName -Add @{"proxyAddresses"="SIP:$($user.UserPrincipalName)"} -Verbose
    }
}