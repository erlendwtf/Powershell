# Export-SwipedOnCSV.ps1
# This short script exports contacts from Active Directory into a CSV format suitable for SwipedOn.com
#
# The Select-Object removes '.' from the "first_name" field, or else you will get an "Illegal Character" warning when uploading your CSV.
# Not sure if this is also needed for the "last_name" - my users don't have any '.' in their last names. :)
# It will also replace + with 00 in phone numbers
# You might have to change the "mobilephone" property in Get-ADUser depending on where you store your phone number values in AD.

$UsersOU = "OU=Users,OU=YourOU,OU=Somewhere,DC=company,DC=tld"
$CSVpath = "\\some\folder\swipedon.csv"
Get-ADUser -SearchBase $UsersOU -Filter {Enabled -eq $true} -Properties givenname, surname, emailaddress, mobilephone | 
Select-Object @{n="first_name";e={($_.givenname).replace(".","")}}, @{n="last_name";e={$_.surname}}, @{n="email";e={$_.emailaddress}}, @{n="phone";e={($_.mobilephone).replace("+","00")}} | 
Export-Csv -Path $CSVpath -Force -NoTypeInformation -Encoding UTF8

